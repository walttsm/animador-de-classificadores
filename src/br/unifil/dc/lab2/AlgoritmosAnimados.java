package br.unifil.dc.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * Write a description of class AlgoritmosAnimados here.
 * 
 * @author Ricardo Inacio
 * @version 20200408
 */
public class AlgoritmosAnimados
{
    /**
     * Grava uma lista estática na tela.
     * @param valores Lista a gravar.
     * @return Animação do metodo.
     */
    public static Gravador listaEstatica(List<Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Valores da lista imutável");
        return anim;
    }

    /**
     * Pesquisa um valor na lista pelo método binário.
     * ATENÇÃO: Este método só funciona com listas ordenadas! No início do método a lista é ordenada automaticamente!
     * @param valores Lista a ser pesquisada.
     * @param chave Inteiro a ser pesquisado.
     * @return Animação da pesquisa.
     */
    public static Gravador pesquisaBinaria(List<Integer> valores, Integer chave){
        classificarPorSelecao(valores);
        Gravador anim = new Gravador(); // instanciacao do gravador
        anim.gravarLista(valores, "Inicio de pesquisa sequencial"); // Gera o quadro de inicio da pesquisa

        int salvaMeio = 0;
        List<Integer> copia = valores;
        int inicioCopia = 0;
        int fimCopia = valores.size() - 1;
        int meioCopia = copia.size() / 2;
        anim.gravarListaPesquisada(valores, inicioCopia, fimCopia, meioCopia);

        do {
            if (chave == copia.get(copia.size() / 2)) {
                salvaMeio += (copia.size() / 2);
                break;
            } else if (chave < copia.get(copia.size() / 2)) {
                fimCopia -= (copia.size() / 2);
                copia = copia.subList(0, copia.size() / 2);
                meioCopia = inicioCopia + copia.size() / 2;
            }
            else {
                salvaMeio += (copia.size() / 2) + 1;
                inicioCopia += (copia.size() / 2) + 1;
                copia = copia.subList((copia.size() / 2) + 1, copia.size());
                meioCopia = inicioCopia + copia.size() / 2;
            }
            anim.gravarListaPesquisada(valores, inicioCopia, fimCopia, meioCopia);
        } while(copia.size() > 0);


        anim.gravarIndiceDestacado(valores, salvaMeio, "Chave encontrada"); // salva o quadro onde a chave foi encontrada
        return anim;
    }

    /**
     * Pesquisa um valor na lista pelo método sequencial.
     * @param valores Lista a ser pesquisada.
     * @param chave Inteiro a ser pesquisado.
     * @return Animação da pesquisa.
     */
    public static Gravador pesquisaSequencial(List<Integer> valores, int chave) {
        Gravador anim = new Gravador(); // instanciacao do gravador
        anim.gravarLista(valores, "Inicio de pesquisa sequencial"); // Gera o quadro de inicio da pesquisa
        
        int i = 0; // Representa o indice onde procuramos o valor chave
        anim.gravarIndiceDestacado(valores, i, "Pesquisa sequencial"); // adiciona um novo quadro onde o primeiro indice é destacado e tem a cor amarela
        while (i < valores.size() && valores.get(i) != chave) { // loop: escapa se chegar no fim da lista ou se o valor for igual a chave...
            i++; // incrementa o indice para selecionar o proximo valor
            anim.gravarIndiceDestacado(valores, i, "Pesquisa sequencial"); // adiciona um novo quadro onde o indice é destacado e tem a cor amarela
        }
        
        if (i < valores.size()) { // checa se o valor onde o indice foi encontrado esta dentro da lista
            anim.gravarIndiceDestacado(valores, i, "Chave encontrada"); // salva o quadro onde a chave foi encontrada
        } else {
            anim.gravarLista(valores, "Chave não encontrada"); // salva o quadro onde nao foi encontrada a chave
        }
        
        return anim; // Devolve os quadros gerados para a tela
    }

    /**
     * Classifica (ordena) a lista pelo método por bolha.
     * @param valores Lista a ser ordenada.
     * @return Animação da ordenação da lista.
     */
    public static Gravador classificarPorBolha(List<Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Disposição inicial");

        boolean troca;
        do {
            anim.gravarLista(valores, "Disposição da lista");
            troca = false;
            for (int i = 1; i < valores.size(); i++) {
                anim.gravarComparacaoSimples(valores, i-1, i);
                if (valores.get(i-1) > valores.get(i)) {
                    Integer copia = valores.get(i-1);
                    valores.set(i-1, valores.get(i));
                    valores.set(i, copia);
                    troca = true;
                    anim.gravarPosTrocas(valores,  i-1, i);
                }
            }
        } while (troca);
        //throw new RuntimeException("Funcionalidade ainda não implementada pelo aluno");
        
        anim.gravarLista(valores, "Disposição final");
        return anim;
    }

    /**
     * Classifica (ordena) a lista pelo método por seleção.
     * @param valores Lista a ser ordenada.
     * @return Animação da ordenação da lista.
     */
    public static Gravador classificarPorSelecao(List<Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Disposicao inicial");

        for (int i = 0; i < valores.size(); i++) {
            anim.gravarIndiceDestacado(valores, i, "Indice que será ordenado.");
            //Percorrendo a lista para encontrar o menor valor.
            Integer menor = valores.get(i);
            int indiceMenor = i;
            for (int j = i + 1; j < valores.size(); j++) {
                anim.gravarMenorValor(valores, i, j,"Procurando menor valor");
                if (valores.get(j) < menor) {
                    menor = valores.get(j);
                    indiceMenor = j;
                }
            }
            anim.gravarMenorValor(valores, i, indiceMenor,"Novo menor valor");
            anim.gravarComparacaoSimples(valores, i, indiceMenor);
            Integer copia = valores.get(i);
            valores.set(i, valores.get(indiceMenor));
            valores.set(indiceMenor, copia);
            anim.gravarPosTrocas(valores, i, indiceMenor);
        }


        anim.gravarLista(valores, "Disposicao final");
        return anim;
    }

    /**
     * Classifica (ordena) a lista pelo método por inserção.
     * @param valores Lista a ser ordenada.
     * @return Animação da ordenação da lista.
     */
    public static Gravador classificarPorInsercao(List<Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Disposicao inicial");

        for (int i = 1; i < valores.size(); i++) {
            Integer numero = valores.get(i);
            int j = i;
            while(j > 0 && valores.get(j-1) > numero) {
                anim.gravarComparacaoSimples(valores, j-1, j);
                valores.set(j, valores.get(j-1));
                anim.gravarPosTrocas(valores, j, j-1);
                j--;
            }
            valores.set(j, numero);
            anim.gravarIndiceDestacado(valores, j, "Item inserido");
        }
        anim.gravarLista(valores, "Disposição final");
        return anim;
    }

    /**
     * Classifica (ordena) a lista pelo método mergesort.
     * @param valores Lista a ser ordenada.
     * @return Animação da ordenação da lista.
     */
    public static Gravador mergesort(List<Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Disposição inicial");

        mergesort(valores, anim);

        anim.gravarLista(valores, "Disposição final");
        return anim;
    }

    /**
     * Classifica (ordena) a lista pelo método quicksort.
     * @param valores Lista a ser ordenada.
     * @return Animação da ordenação da lista.
     */
    public static Gravador quicksort (List<Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Disposição inicial");

        quicksort(anim, valores, 0, valores.size() - 1);

        anim.gravarLista(valores, "Disposição final");
        return anim;
    }

    /**
     * Método auxiliar que cuida do processo recursivo do mergesort.
     * @param valores Lista passada ao mergesort.
     * @param anim Instância de gravador criado no método público.
     */
    private static void mergesort(List<Integer> valores, Gravador anim) {
        if (valores.size() <= 1) return;
        anim.gravarLista(valores, "Disposição atualizada");
        // Dividindo recursivamente
        List<Integer> esquerda = new ArrayList<>(valores.subList(0, valores.size() / 2));
        anim.gravarListaPesquisada(valores, 0, (valores.size() / 2) - 1);
        mergesort(esquerda, anim);

        List<Integer> direita = new ArrayList<>(valores.subList(valores.size() / 2, valores.size()));
        anim.gravarListaPesquisada(valores, valores.size() / 2, valores.size() - 1);
        mergesort(direita,anim);

        merge(anim, valores, esquerda, direita);
        anim.gravarLista(valores, "Disposição atualizada");
    }

    /**
     * Método auxiliar que cuida da parte de ordenação e junção das partes da lista por mergesort.
     * @param anim Instância de gravador criado no método público.
     * @param valores Lista passada ao mergesort.
     * @param esquerda Metade esquerda da lista ordenada.
     * @param direita Metade direita da lista ordenada.
     */
    private static void merge(Gravador anim, List<Integer> valores, List<Integer> esquerda, List<Integer> direita) {
        int iEsquerda = 0;
        int iDireita = 0;
        int iLista = 0;

        List<Integer> copia = new ArrayList<>(esquerda);
        copia.addAll(direita);
        while (iEsquerda < esquerda.size() && iDireita < direita.size()) {// Enquanto nenhuma lista acabar
            anim.gravarComparacaoCopia(copia,
                                       pesquisaValor(copia,esquerda.get(iEsquerda)),
                                       pesquisaValor(copia, direita.get(iDireita)));

            if (esquerda.get(iEsquerda) <= direita.get(iDireita)) {// Se o numero da pilha esquerda for menor
                valores.set(iLista, esquerda.get(iEsquerda)); // A Lista junta recebe o valor
                iEsquerda++; // Atualizamos o indice da parte esquerda
            } else { // Se o numero da pilha direita for menor
                valores.set(iLista, direita.get(iDireita));
                iDireita++;
            }

            anim.gravarIndiceDestacado(valores, iLista, "Valor inserido");
            iLista++;
        }

        // Acabaram os valores de uma das listas.
        List<Integer> sobra;
        int iSobra = 0;
        if (iEsquerda < esquerda.size()) {
            sobra = esquerda;
            iSobra = iEsquerda;
        } else {
            sobra = direita;
            iSobra = iDireita;
        }

        while (iSobra < sobra.size()) {
            valores.set(iLista, sobra.get(iSobra));
            anim.gravarIndiceDestacado(valores, iLista, "Valor inserido");
            iSobra++;
            iLista++;
        }
    }

    /**
     * Pesquisa sequencialmente um valor numa lista.
     * Criado como método auxiliar para a gravação do mergesort.
     * @param valores Lista de procura.
     * @param chave Valor a ser procurado.
     * @return Indice da chave.
     */
    private static int pesquisaValor(List<Integer> valores, int chave) {
        int i = 0; // Representa o indice onde procuramos o valor chave
        while (i < valores.size() && valores.get(i) != chave) { // loop: escapa se chegar no fim da lista ou se o valor for igual a chave...
            i++; // incrementa o indice para selecionar o proximo valor
        }
        return i;
    }

    /**
     * Método auxiliar que cuida da parte recursiva do quicksort.
     * @param anim Instância de gravador criado no método público.
     * @param valores Lista passada ao quicksort.
     * @param inicio Inicio da lista valores.
     * @param fim Fim da lista valores.
     */
    private static void quicksort(Gravador anim, List<Integer> valores, int inicio, int fim) {
        if (inicio >= fim) return;
        anim.gravarLista(valores, "Lista atual");
        int pivo = valores.get((inicio + fim) / 2);
        int idxPivo = dividir(anim, valores,inicio, fim, pivo);
        quicksort(anim, valores, inicio, idxPivo - 1);
        quicksort(anim, valores, idxPivo, fim);
    }

    /**
     * Método auxiliar ao quicksort que organiza os itens da lista em relação ao pivô.
     * @param anim Instância de gravador criado no método público.
     * @param valores Lista passada ao quicksort.
     * @param inicio Inicio da lista valores.
     * @param fim Fim da lista valores.
     * @param pivo Valor do pivô usado como referência.
     * @return indice final de ordenação;
     */
    private static int dividir(Gravador anim, List<Integer> valores, int inicio, int fim, int pivo) {
        while (inicio <= fim) {
            anim.gravarListaPesquisada(valores,inicio,fim, valores.indexOf(pivo));
            while (valores.get(inicio) < pivo) {
                inicio++;
                anim.gravarListaPesquisada(valores, inicio, fim, valores.indexOf(pivo));
            }
            while (valores.get(fim) > pivo) {
                fim--;
                anim.gravarListaPesquisada(valores, inicio, fim, valores.indexOf(pivo));

            }
            if (inicio <= fim) {
                if (inicio == valores.indexOf(pivo) || fim == valores.indexOf(pivo)) {
                    anim.gravarListaPesquisada(valores, inicio, fim);
                }
                trocar(valores, inicio, fim);
                anim.gravarPosTrocas(valores, inicio, fim);
                inicio++;
                fim--;
            }
        }
        return inicio;
    }

    /**
     * Método auxiliar ao método dividir. Faz a troca de posição de dois valores.
     * @param valores Lista onde ocorre a troca.
     * @param idx1 Indice do primeiro valor da troca.
     * @param idx2 Indice do segundo valor da troca.
     */
    private static void trocar(List<Integer> valores, int idx1, int idx2) {
        Integer copia = valores.get(idx1);
        valores.set(idx1, valores.get(idx2));
        valores.set(idx2, copia);

    }
}