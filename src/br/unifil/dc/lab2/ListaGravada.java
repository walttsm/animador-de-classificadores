package br.unifil.dc.lab2;

import javax.swing.JPanel;
import java.awt.*;
import java.util.List;
import java.util.Random;

//Lista = 255,135,450,315,90

/**
 * Write a description of class ListaGravada here.
 * 
 * @author Ricardo Inacio
 * @version 20200409
 */
public class ListaGravada implements Transparencia
{
    /**
     * Constructor for objects of class ListaGravada
     */
    public ListaGravada(List<Integer> lista, List<Color> coresIndices, String nome) {
        this.lista = lista;
        this.nome = nome;
        this.coresIndices = coresIndices;
    }

    /**
     * Pinta as colunas a tela de acordo com seus valores.
     * @param pincel Ferramenta de pintura.
     * @param contexto Tamanho da tela.
     */
    public void pintar(Graphics2D pincel, JPanel contexto) {
        int x = 0;
        int biggestValue = getBiggestValue(this.lista);
        Dimension dim = contexto.getSize();
        float totalWidth = (float) dim.width / this.lista.size();
        float columnWidth = 0.5f * totalWidth;

        for (int i = 0; i < this.lista.size(); i++) {
            int value = this.lista.get(i);
            Color color = this.coresIndices.get(i);
            if (this.coresIndices.get(i) == null) {color = Color.BLUE;}
            if (i == 0) x += 20;
            else x += totalWidth;

            desenharColuna(pincel, x, value, columnWidth, color,biggestValue);
            desenharValue(pincel, x, columnWidth, value);
        }

        //throw new RuntimeException("Funcionalidade ainda não implementada pelo aluno");
    }

    /**
     * Busca o maior valor da lista.
     * Caso existam apenas valores negativos na lista, retorna valor 0;
     * @return O maior valor da lista.
     */
    private int getBiggestValue(List<Integer> lista) {
        int biggest = 0;
        for (int value:
             lista) {
            if (value > biggest) biggest = value;
        }
        return biggest;
    }

    /**
     * Desenha um retangulo nas posicoes e tamanhos nas posicoes da lista
     * @param pincel Ferramenta de desenho
     */
    private void pintarDesenhoAleatorio(Graphics2D pincel) {
        pincel.drawRect(lista.get(0),lista.get(1),lista.get(2),lista.get(3));
    }

    /**
     * Desenha a coluna na tela.
     * @param pincel Ferramenta de desenho
     * @param x Posicao x onde se desenhara a coluna.
     * @param width Largura da coluna.
     * @param value Valor armazenado no objeto coluna.
     * @param biggestValue Maior valor da lista para comparação
     */
    private void desenharColuna(Graphics2D pincel, int x, int value, float width, Color color, int biggestValue){
        Coluna coluna = new Coluna(value , width, color);
        int colunaHeight = (int) coluna.getActualHeight(biggestValue);
        pincel.setColor(Color.BLACK);
        pincel.setStroke(new BasicStroke(10));
        pincel.drawRect(x, 500 - colunaHeight, (int) coluna.getWidth(), colunaHeight);
        pincel.setColor(coluna.getColor());
        pincel.fillRect(x, 500 - colunaHeight, (int) coluna.getWidth(), colunaHeight);
        pincel.setStroke(new BasicStroke(2));
        pincel.setColor(Color.BLACK);
    }

    /**
     * Desenha o valor da coluna abaixo dela.
     * @param pincel Ferramenta de desenho
     * @param x Posicao x onde se desenhara a coluna.
     * @param width Largura da coluna.
     * @param value Valor armazenado no objeto coluna.
     */
    private void desenharValue(Graphics2D pincel,int x, float width, Integer value) {
        pincel.setFont(new Font(pincel.getFont().toString(),Font.BOLD,12));
        pincel.drawString(value.toString(),x + (width/10), 520);
    }
    
    
    private List<Integer> lista;
    private List<Color> coresIndices;
    private String nome;
}
