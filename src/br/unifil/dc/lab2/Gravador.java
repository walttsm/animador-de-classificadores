package br.unifil.dc.lab2;

import java.util.List;
import java.util.ListIterator;
import java.util.ArrayList;
import java.awt.Color;
import java.util.ListResourceBundle;

/**
 * Write a description of class Gravador here.
 *
 * @author Ricardo Inacio
 * @version 20200409
 */
public class Gravador
{
    /**
     * Constructor of object gravador
     */
    public Gravador() {
        this.seqGravacoes = new ArrayList<Transparencia>();
    }

    /**
     * Gera quadros a partir de uma lista.
     * @param lista lista que é gravada no quadro.
     * @param nome nome identificador para a lista atual.
     */
    public void gravarLista(List<Integer> lista, String nome) {
        List<Color> cores = novaListaColors(lista.size(), Color.BLUE);
        ListaGravada gravacao = new ListaGravada(List.copyOf(lista), cores, nome);
        seqGravacoes.add(gravacao);
    }

    /**
     * Grava um quadro com o indice demarcado na cor amarela.
     * @param lista lista gravada no quadro.
     * @param i indice que esta sendo marcado.
     * @param nome nome identificador para a lista.
     */
    public void gravarIndiceDestacado(List<Integer> lista, int i, String nome) {
        List<Color> cores = novaListaColors(lista.size(), Color.BLUE);
        cores.set(i, Color.YELLOW);
        ListaGravada gravacao = new ListaGravada(List.copyOf(lista), cores, nome);
        seqGravacoes.add(gravacao);
    }

    /**
     * Grava um quadro com o valor atual de checagem durante a classficação (amarelo) e o menor valor da lista(vermelho).
     * @param lista lista do quadro.
     * @param i indice atual de checagem.
     * @param j indice do menor valor da lista.
     * @param nome Nome para a gravação.
     */
    public void gravarMenorValor(List<Integer> lista, int i, int j, String nome) {
        List<Integer> copia = new ArrayList<Integer>(lista);
        List<Color> cores = novaListaColors(lista.size(),Color.BLUE);
        cores.set(i, Color.YELLOW);
        cores.set(j, Color.RED);
        ListaGravada gravacao = new ListaGravada(copia, cores, nome);
        seqGravacoes.add(gravacao);
    }

    /**
     * Grava um quadro durante a comparação de dois valores.
     * @param lista lista gravada no quadro.
     * @param i indice do primeiro valor da comparação.
     * @param j indice do segundo valor da comparação.
     */
    public void gravarComparacaoSimples(List<Integer> lista, int i, int j) {
        List<Color> cores = novaListaColors(lista.size(), Color.BLUE);
        cores.set(i, Color.GRAY);
        cores.set(j, Color.GRAY);
        ListaGravada gravacao = new ListaGravada(List.copyOf(lista), cores, "Comparação");
        seqGravacoes.add(gravacao);
    }

    /**
     * Grava um quadro durante a comparação de dois valores durante o mergesort.
     * @param lista lista de valores das metades sendo juntadas
     * @param i indice do primeiro valor da comparação.
     * @param j indice do segundo valor da comparação.
     */
    public void gravarComparacaoCopia(List<Integer> lista, int i, int j) {
        List<Color> cores = novaListaColors(lista.size(), Color.CYAN);
        cores.set(i, Color.GRAY);
        cores.set(j, Color.GRAY);
        ListaGravada gravacao = new ListaGravada(List.copyOf(lista), cores, "Comparação");
        seqGravacoes.add(gravacao);
    }

    /**
     * Faz a gravação de um quadro após a troca entre dois valores e os marca com a cor amarela por um quadro.
     * @param lista lista gravada no quadro
     * @param i primeiro indice trocado.
     * @param j segundo indice trocado.
     */
    public void gravarPosTrocas(List<Integer> lista, int i, int j) {
        List<Color> cores = novaListaColors(lista.size(), Color.BLUE);
        cores.set(i, Color.YELLOW);
        cores.set(j, Color.YELLOW);
        ListaGravada gravacao = new ListaGravada(List.copyOf(lista), cores, "Pós-troca");
        seqGravacoes.add(gravacao);
    }

    /**
     * Grava a lista que será organizada com a cor vermelha no indice de inicio e no indice final da divisão.
     * @param lista lista em sua totalidade.
     * @param inicio inicio da sublista.
     * @param fim fim da sublista.
     */
    public void gravarListaPesquisada(List<Integer> lista, int inicio, int fim) {
        List<Color> cores = novaListaColors(lista.size(), Color.BLUE);
        cores.set(inicio, Color.RED);
        cores.set(fim, Color.RED);
        ListaGravada gravacao = new ListaGravada(List.copyOf(lista), cores, "Comparação");
        seqGravacoes.add(gravacao);
    }

    /**
     * Grava a lista que será organizada com a cor vermelha no indice de inicio e no indice final da divisão.
     * IMPORTANTE: o valor 'meio' representa o indice de comparação!
     * @param lista lista em sua totalidade.
     * @param inicio inicio da sublista.
     * @param fim fim da sublista.
     * @param meio indice de comparação.
     */
    public void gravarListaPesquisada(List<Integer> lista, int inicio, int fim, int meio){
        List<Integer> copia = new ArrayList<>(lista);
        List<Color> cores = novaListaColors(lista.size(), Color.BLUE);
        cores.set(inicio, Color.GRAY);
        cores.set(fim, Color.GRAY);
        cores.set(meio, Color.RED);
        ListaGravada gravacao = new ListaGravada(copia, cores, "Lista de pesquisa atual");
        seqGravacoes.add(gravacao);
    }



    /**
     * Permite a navegação pela lista de quadros gerado nos métodos acima.
     * @return A lista de gravações como uma lista de quadros para serem exibidos na tela.
     */
    public ListIterator<Transparencia> getFilme() {
        return seqGravacoes.listIterator();
    }

    /**
     * Cria uma nova lista de cores do mesmo tamanho da lista de valores.
     * @param numElems numero de elementos para criacao da lista.
     * @return Cor para criar a lista.
     */
    private static List<Color> novaListaColors(int numElems, Color cor) {
        ArrayList<Color> lista = new ArrayList<>(numElems);
        for (; numElems > 0; numElems--) lista.add(cor);
        return lista;
    }

    private List<Transparencia> seqGravacoes;
}
