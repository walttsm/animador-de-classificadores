package br.unifil.dc.lab2;

import java.awt.*;

public class Coluna {

    /**
     * Construtor do objeto coluna.
     * @param value valor da coluna
     * @param width Largura da coluna.
     * @param color Cor da coluna.
     */
    public Coluna(int value, float width, Color color) {
        this.value = value;
        this.width = width;
        this.color = color;
    }

    /**
     * Retorna o valor da coluna.
     * @return Valor da coluna
     */
    public int getValue() {
        return value;
    }

    /**
     * Retorna a largura da coluna.
     * @return Largura da coluna.
     */
    public float getWidth() {
        return width;
    }

    /**
     * Retorna a cor da coluna.
     * @return Cor da coluna
     */
    public Color getColor() { return color; }

    /**
     * Calcula o valor da altura da coluna em pixels.
     * A altura máxima é 300 pixels, todas as outras colunas obterão um valor menor.
     * @param biggestValue Maior valor da lista que gera as colunas.
     * @return Altura da coluna em pixels, onde 0 < Altura >= 300 pixels.
     */
    public float getActualHeight(int biggestValue) {
        return (((float) this.value)/ ((float) biggestValue)) * 300f;
    }



    private int value;
    private float width;
    private Color color;
}
